/*
* Combining operators
* Operators that work with multiple source Observables to create a single Observable
*/

import { of, timer, combineLatest, interval, forkJoin, merge, zip } from 'rxjs'
import { startWith, take, map, combineAll, switchMap } from 'rxjs/operators'
import print from './utils/print'

// startWith - Returns an Observable that emits the items you specify
// as arguments before it begins to emit items emitted by the source Observable.
// https://rxjs-dev.firebaseapp.com/api/operators/startWith

const StartWith = () => {
  of('from source')
    .pipe(startWith('first', 'second'))
    .subscribe(x => print('StartWith: ', x))
}

// CombineLatest - combines multiple Observables to create an Observable
// whose values are calculated from the latest values of each
// of its input Observables.
// https://rxjs-dev.firebaseapp.com/api/operators/combineLatest

const CombineLatest = () => {
  const firstTimer = timer(0, 200).pipe(take(3))
  const secondTimer = timer(100, 200).pipe(take(3))
  const combinedTimers = combineLatest(firstTimer, secondTimer)
  combinedTimers.subscribe(value => print('CombineLatest: ', value))
}

// CombineAll - flattens an Observable-of-Observables by applying
// combineLatest when the Observable-of-Observables completes.
// https://rxjs-dev.firebaseapp.com/api/operators/combineAll

const CombineAll = () => {
  const numbers = of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
  const higherOrder = numbers.pipe(
    map(() => interval(Math.random() * 10).pipe(take(3))),
    take(2)
  )
  const result = higherOrder.pipe(combineAll())

  result.subscribe(x => print('CombineAll', x))
}

// ForkJoin - joins last values emitted by passed Observables.
// https://rxjs-dev.firebaseapp.com/api/operators/forkJoin

const ForkJoin = () => {
  const observable = forkJoin(of(1, 2, 3, 4), of(5, 6, 7, 8))
  observable.subscribe(
    value => print('ForkJoin: ', value),
    err => print('ForkJoin: ', err),
    () => print('ForkJoin: ', 'This is how it ends!')
  )
}

// Merge - creates an output Observable which concurrently
// emits all values from every given input Observable.
// https://rxjs-dev.firebaseapp.com/api/operators/merge

const Merge = () => {
  const numbers = of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
  const timerRes = interval(100).pipe(take(3))
  const clicksOrTimer = merge(numbers, timerRes)
  clicksOrTimer.subscribe(x => print('Merge: ', x))
}

// SwitchMap - projects each source value to an Observable which is
// merged in the output Observable, emitting values only from
// the most recently projected Observable.
// https://rxjs-dev.firebaseapp.com/api/operators/switchMap

const SwitchMap = () => {
  const numbers = of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
  const result = numbers.pipe(switchMap(() => interval(100).pipe(take(3))))
  result.subscribe(x => print('Switch: ', x))
}

// Zip - combines multiple Observables to create an Observable whose
// values are calculated from the values, in order, of each of
// its input Observables.
// https://rxjs-dev.firebaseapp.com/api/operators/zip

const Zip = () => {
  const age$ = of(27, 25, 29)
  const name$ = of('Foo', 'Bar', 'Beer')
  const isDev$ = of(true, true, false)

  zip(age$, name$, isDev$)
    .pipe(map((age, name, isDev) => ({ age, name, isDev })))
    .subscribe(x => print('Zip: ', x))
}

StartWith()
CombineLatest()
CombineAll()
ForkJoin()
Merge()
SwitchMap()
Zip()
