/*
* Creating Observables
* Operators that originate new Observables.
*/

import {
  Observable,
  defer,
  interval,
  EMPTY,
  NEVER,
  throwError,
  concat,
  of,
  from,
  range,
  timer
} from 'rxjs'
import { repeat, startWith, take } from 'rxjs/operators'
import print from './utils/print'

// Create — create an Observable from scratch
// by calling observer methods programmatically
// http://reactivex.io/documentation/operators/create.html

const Create = () => {
  const source = Observable.create(observer => {
    print('Create: created')
    observer.next(42)
    observer.complete()
    // Note that this is optional, you do not have to return this if you require no cleanup
    return () => print('Create: disposed')
  })

  source.subscribe(
    x => print(`Create: next: ${x}`),
    err => print(`Create: error: ${err}`),
    () => print('Create: completed')
  )
}

// Defer - do not create the Observable until the observer subscribes,
// and create a fresh Observable for each observer
// http://reactivex.io/documentation/operators/defer.html

const Defer = () => {
  const intervalObservable = defer(() => interval(25))
  const subscribtion = intervalObservable.subscribe(x => print('Defer: ', x))
  setTimeout(() => subscribtion.unsubscribe(), 100)
}

// Empty - Creates an Observable that emits no items to the Observer
// and immediately emits a complete notification.
// EMPTY - The same Observable instance returned by any call to empty without a scheduler.
// It is preferrable to use this over empty().
// https://rxjs-dev.firebaseapp.com/api/index/function/empty

const Empty = () => {
  const result = EMPTY.pipe(startWith(7))
  result.subscribe(x => print('Empty: ', x))
}

// NEVER - An Observable that emits no items to the Observer and never completes.
// https://rxjs-dev.firebaseapp.com/api/index/const/NEVER

const Never = () => {
  const info = () => print('Will not be called')

  const result = NEVER.pipe(startWith(7))
  result.subscribe(x => print('Never: ', x), info, info)
}

// throwError - Creates an Observable that emits no items to the Observer
// and immediately emits an error notification.
// https://rxjs-dev.firebaseapp.com/api/index/function/throwError

const ThrowError = () => {
  const result = concat(
    of(7),
    throwError(new Error('Throw Error: oops!')),
    of(7) // will not be called
  )
  result.subscribe(
    x => print('ThrowError: ', x),
    e => console.error('ThroeError: ', e)
  )
}

// from - Creates an Observable from an Array, an array-like object,
// a Promise, an iterable object, or an Observable-like object.
// https://rxjs-dev.firebaseapp.com/api/index/function/from

const From = () => {
  const array = [10, 20, 30]
  const result = from(array)

  result.subscribe(x => print('From: ', x))
}

// interval - Creates an Observable that emits sequential numbers
// every specified interval of time, on a specified SchedulerLike.
// https://rxjs-dev.firebaseapp.com/api/index/function/interval

const Interval = () => {
  const numbers = interval(100)
  const takeFourNumbers = numbers.pipe(take(4))

  takeFourNumbers.subscribe(x => print('Interval: ', x))
}

// range - Creates an Observable that emits a sequence of numbers
// within a specified range.
// https://rxjs-dev.firebaseapp.com/api/index/function/interval

const Range = () => {
  const numbers = range(1, 5)
  numbers.subscribe(x => print('Range: ', x))
}

// repeat - Returns an Observable that repeats the stream of items
// emitted by the source Observable at most count times.
// https://rxjs-dev.firebaseapp.com/api/operators/repeat

const Repeat = () => {
  const numbers = range(1, 2).pipe(repeat(2))
  numbers.subscribe(x => print('Repeat: ', x))
}

// timer - Creates an Observable that starts emitting after an dueTime
// and emits ever increasing numbers after each period of time thereafter.
// https://rxjs-dev.firebaseapp.com/api/operators/repeat

const Timer = () => {
  const timerNumbers = timer(200, 100)
  const takeTimer = timerNumbers.pipe(take(3))
  takeTimer.subscribe(x => print('Timer: ', x))
}

Create()
Defer()
Empty()
Never()
ThrowError()
From()
Interval()
Range()
Repeat()
Timer()
