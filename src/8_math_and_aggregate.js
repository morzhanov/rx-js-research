/*
* Mathematical and Aggregate Operators
* Operators that operate on the entire sequence of items emitted by an Observable
*/

import { of, interval, from, range, concat } from 'rxjs'
import {
  defaultIfEmpty,
  sequenceEqual,
  bufferCount,
  count,
  take,
  mapTo,
  reduce
} from 'rxjs/operators'
import print from './utils/print'

// Concat - creates an output Observable which sequentially emits all
// values from given Observable and then moves on to the next.
// https://rxjs-dev.firebaseapp.com/api/operators/concat

const Concat = () => {
  const timer = interval(100).pipe(take(4))
  const sequence = range(1, 10)
  const result = concat(timer, sequence)
  result.subscribe(x => print('Concat: ', x))
}

// Count - counts the number of emissions on the source
// and emits that number when the source completes.
// https://rxjs-dev.firebaseapp.com/api/operators/count

const Count = () => {
  const numbers = range(1, 7)
  const result = numbers.pipe(count(i => i % 2 === 1))
  result.subscribe(x => print('Count: ', x))
}

// Reduce - applies an accumulator function over the source Observable,
// and returns the accumulated result when the source completes,
// given an optional seed value.
// https://rxjs-dev.firebaseapp.com/api/operators/reduce

const Reduce = () => {
  const source = of(1, 2, 3, 4)
  const example = source.pipe(reduce((acc, val) => acc + val))
  example.subscribe(val => print('Reduce: Sum:', val))
}

Concat()
Count()
Reduce()
