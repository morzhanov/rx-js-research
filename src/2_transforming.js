/*
* Transforming operators
* Operators that transform items that are emitted by an Observable.
*/

import { interval, from, of, merge } from 'rxjs'
import {
  buffer,
  flatMap,
  mergeMap,
  map,
  take,
  groupBy,
  reduce,
  mapTo,
  scan
} from 'rxjs/operators'
import print from './utils/print'

// Buffer — Buffers the source Observable values until closingNotifier emits.
// https://rxjs-dev.firebaseapp.com/api/operators/buffer

const BufferExample = () => {
  const array = [10, 20, 30]
  const result = from(array)
  const intervals = interval(1000)
  const buffered = intervals.pipe(buffer(result))
  buffered.subscribe(x => print('Buffer:', x))
}

// FlatMap - Projects each source value to an Observable which
// is merged in the output Observable.
// https://rxjs-dev.firebaseapp.com/api/operators/flatMap

const FlatMap = () => {
  const letters = of('a', 'b', 'c')
  const result = letters.pipe(
    flatMap(x =>
      interval(100).pipe(
        take(3),
        map(i => x + i)
      )
    )
  )
  result.subscribe(x => print('FlatMap: ', x))
}

// MergeMap - Projects each source value to an Observable which
// is merged in the output Observable.
// https://rxjs-dev.firebaseapp.com/api/operators/mergeMap

const MergeMap = () => {
  const letters = of('a', 'b', 'c')
  const result = letters.pipe(
    mergeMap(x =>
      interval(100).pipe(
        take(3),
        map(i => x + i)
      )
    )
  )
  result.subscribe(x => print('MergeMap: ', x))
}

// GroupBy - Groups the items emitted by an Observable according to
// a specified criterion, and emits these grouped items as GroupedObservables,
// one GroupedObservable per group.
// https://rxjs-dev.firebaseapp.com/api/operators/groupBy

const GroupBy = () => {
  of(
    { id: 1, name: 'javascript' },
    { id: 2, name: 'parcel' },
    { id: 2, name: 'webpack' },
    { id: 1, name: 'typescript' },
    { id: 3, name: 'tslint' }
  )
    .pipe(
      groupBy(p => p.id),
      mergeMap(group$ => group$.pipe(reduce((acc, cur) => [...acc, cur], [])))
    )
    .subscribe(p => print('GroupBy: ', p))
}

// Map - Applies a given project function to each value emitted by the
// source Observable, and emits the resulting values as an Observable.
// https://rxjs-dev.firebaseapp.com/api/operators/map

const Map = () => {
  const numbers = of(0, 1, 2)
  const positions = numbers.pipe(map(ev => ev * 1000))
  positions.subscribe(x => print('Map: ', x))
}

// Scan - Applies an accumulator function over the source Observable, and returns
// each intermediate result, with an optional seed value.
// https://rxjs-dev.firebaseapp.com/api/operators/scan

const Scan = () => {
  const numbers = of(0, 1, 2)
  const ones = numbers.pipe(mapTo(1))
  const seed = 0
  const count = ones.pipe(scan((acc, one) => acc + one, seed))
  count.subscribe(x => print('Scan: ', x))
}

// Merge - Creates an output Observable which concurrently
// emits all values from every given input Observable.
// https://rxjs-dev.firebaseapp.com/api/index/function/merge

const Merge = () => {
  const numbers = of(0, 1, 2)
  const timer = interval(100)
  const clicksOrTimer = merge(numbers, timer).pipe(take(3))
  clicksOrTimer.subscribe(x => print('Merge: ', x))
}

BufferExample()
FlatMap()
MergeMap()
GroupBy()
Map()
Scan()
Merge()
