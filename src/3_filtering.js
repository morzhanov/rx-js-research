/*
* Filtering operators
* Operators that selectively emit items from a source Observable.
*/

import { interval, from, of } from 'rxjs'
import {
  debounce,
  distinct,
  elementAt,
  filter,
  first,
  ignoreElements,
  last
} from 'rxjs/operators'
import print from './utils/print'

// Debounce — emits a value from the source Observable only
// after a particular time span determined by another Observable
// has passed without another source emission.
// https://rxjs-dev.firebaseapp.com/api/operators/debounce

const Debounce = () => {
  const array = [10, 20, 30]
  const numbers = from(array)
  const result = numbers.pipe(debounce(() => interval(100)))
  result.subscribe(x => print('Debounce: ', x))
}

// Distinct — returns an Observable that emits all items
// emitted by the source Observable that are distinct by
// comparison from previous items.
// https://rxjs-dev.firebaseapp.com/api/operators/distinct

const Distinct = () => {
  of(1, 1, 2, 2, 2, 1, 2, 3, 4, 3, 2, 1)
    .pipe(distinct())
    .subscribe(x => print('Distinct: ', x))
}

// ElementAt — emits the single value at the specified index
// in a sequence of emissions from the source Observable.
// https://rxjs-dev.firebaseapp.com/api/operators/elementAt

const ElementAt = () => {
  const numbers = of(1, 6, 7, 4, 8, 2, 0, 9, 3, 1, 0, 1)
  const result = numbers.pipe(elementAt(2))
  result.subscribe(x => print('ElementAt: ', x))
}

// Filter — filter items emitted by the source Observable
// by only emitting those that satisfy a specified predicate.
// https://rxjs-dev.firebaseapp.com/api/operators/filter

const Filter = () => {
  const numbers = of(1, 6, 7, 4, 8, 2, 0, 9, 3, 1, 0, 1)
  const numbersMoreThree = numbers.pipe(filter(number => number > 3))
  numbersMoreThree.subscribe(x => print('Filter: ', x))
}

// First — emits only the first value (or the first value
// that meets some condition) emitted by the source Observable.
// https://rxjs-dev.firebaseapp.com/api/operators/first

const First = () => {
  const numbers = of(111, 6, 7, 4, 8, 2, 0, 9, 3, 1, 0, 1)
  const result = numbers.pipe(first())
  result.subscribe(x => print('First: ', x))
}

// IgnoreElements — ignores all items emitted by the source
// Observable and only passes calls of complete or error.
// https://rxjs-dev.firebaseapp.com/api/operators/ignoreElements

const IgnoreElements = () => {
  of('you', 'talking', 'to', 'me')
    .pipe(ignoreElements())
    .subscribe(
      word => print('IgnoreElements: ', word),
      err => print('IgnoreElements: ', 'error:', err),
      () => print('IgnoreElements: ', 'the end')
    )
}

// Last — returns an Observable that emits only the last item
// emitted by the source Observable. It optionally takes a predicate
// function as a parameter, in which case, rather than emitting
// the last item from the source Observable, the resulting Observable will
// emit the last item from the source Observable that satisfies the predicate.
// https://rxjs-dev.firebaseapp.com/api/operators/last

const Last = () => {
  const numbers = of(111, 6, 7, 4, 8, 2, 0, 9, 3, 1, 0, 111)
  const result = numbers.pipe(last())
  result.subscribe(x => print('Last: ', x))
}

Debounce()
Distinct()
ElementAt()
Filter()
First()
IgnoreElements()
Last()
