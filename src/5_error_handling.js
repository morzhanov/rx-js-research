/*
* Error Handling operators
* Operators that help to recover from error notifications from an Observable
*/

import { of, throwError, interval } from 'rxjs'
import { take, catchError, mergeMap, retry } from 'rxjs/operators'
import print from './utils/print'

// CartchError - gracefully handle errors in an observable sequence.
// https://rxjs-dev.firebaseapp.com/api/operators/catchError

const CartchError = () => {
  const source = throwError('This is an error!')
  const example = source.pipe(catchError(val => of(`I caught: ${val}`)))
  example.subscribe(val => print('CatchError: ', val))
}

// Retry - returns an Observable that mirrors the source Observable
// with the exception of an error. If the source Observable calls error,
// this method will resubscribe to the source Observable for a maximum of count
// resubscriptions (given as a number parameter) rather than propagating
// the error call.
// https://rxjs-dev.firebaseapp.com/api/operators/retry

const Retry = () => {
  const source = interval(100)
  const example = source.pipe(
    mergeMap(val => {
      if (val > 2) {
        return throwError('Error!')
      }
      return of(val)
    }),
    retry(2)
  )

  example.subscribe({
    next: val => print('Retry: ', val),
    error: val => print('Retry: ', `${val}: Retried 2 times then quit!`)
  })
}

CartchError()
Retry()
