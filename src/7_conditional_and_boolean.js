/*
* Conditional and Boolean operators
* Operators that evaluate one or more Observables or items emitted by Observables
*/

import { of, interval, from } from 'rxjs'
import {
  defaultIfEmpty,
  takeUntil,
  map,
  sequenceEqual,
  bufferCount,
  mergeMap
} from 'rxjs/operators'
import print from './utils/print'

// DefaultIfEmpty - delays the emission of items from the source Observable
// by a given timeout or until a given Date.
// https://rxjs-dev.firebaseapp.com/api/operators/defaultIfEmpty

const DefaultIfEmpty = () => {
  const numbers = of(0, 1, 2, 3)
  const numbersBeforeFive = numbers.pipe(takeUntil(interval(10)))
  const result = numbersBeforeFive.pipe(defaultIfEmpty('no clicks'))
  result.subscribe(x => print('DefaultEmpty: ', x))
}

// SequenceEqual - compares all values of two observables in sequence
// using an optional comparor function and returns an observable
// of a single boolean value representing whether or not the
// two sequences are equal.
// https://rxjs-dev.firebaseapp.com/api/operators/defaultIfEmpty

const SequenceEqual = () => {
  const codes = of(0, 1, 2, 3, 4, 5)

  const keys = of(0, 1, 2, 3, 4, 5).pipe(map(e => e))
  const matches = keys.pipe(
    bufferCount(11, 1),
    mergeMap(last11 => from(last11).pipe(sequenceEqual(codes)))
  )
  matches.subscribe(matched =>
    print('SequenceEqual: ', 'Successful cheat at Contra? ', matched)
  )
}

DefaultIfEmpty()
SequenceEqual()
