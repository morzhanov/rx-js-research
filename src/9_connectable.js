/*
* Connectable Observable Operators
* Specialty Observables that have more precisely-controlled subscription dynamics
*/

import { interval } from 'rxjs'
import { publish, tap, take } from 'rxjs/operators'
import print from './utils/print'

// Publish - Share source and make hot by calling connect.
// https://rxjs-dev.firebaseapp.com/api/operators/publish

const Publish = () => {
  const source = interval(100).pipe(take(2))
  const example = source.pipe(
    // side effects will be executed once
    tap(() => print('Publish: Do Something!')),
    // do nothing until connect() is called
    publish()
  )

  example.subscribe(val => print(`Publish: Subscriber One: ${val}`))
  example.subscribe(val => print(`Publish: Subscriber Two: ${val}`))

  setTimeout(() => {
    example.connect()
  }, 100)
}

Publish()
