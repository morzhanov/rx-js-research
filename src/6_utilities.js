/*
* Error Handling operators
* Operators that help to recover from error notifications from an Observable
*/

import { of, interval, Notification } from 'rxjs'
import {
  take,
  delay,
  map,
  materialize,
  dematerialize,
  timeout
} from 'rxjs/operators'
import print from './utils/print'

// Delay - delays the emission of items from the source Observable
// by a given timeout or until a given Date.
// https://rxjs-dev.firebaseapp.com/api/operators/delay

const Delay = () => {
  const numbers = of(0, 1, 2, 3)
  const delayedClicks = numbers.pipe(delay(300))
  delayedClicks.subscribe(x => print('Delay:', x))
}

// Materialize - represents all of the notifications from the source
// Observable as next emissions marked with their original
// types within Notification objects.
// https://rxjs-dev.firebaseapp.com/api/operators/materialize

const Materialize = () => {
  const letters = of('a', 'b', 'c', 'd')
  const upperCase = letters.pipe(map(x => x.toUpperCase()))
  const materialized = upperCase.pipe(materialize())
  materialized.subscribe(x => print('Materialize: ', x))
}

// Dematerialize - donverts an Observable of Notification objects
// into the emissions that they represent.
// https://rxjs-dev.firebaseapp.com/api/operators/dematerialize

const Dematerialize = () => {
  const notifA = new Notification('N', 'A')
  const notifB = new Notification('N', 'B')
  const notifE = new Notification(
    'E',
    undefined,
    new TypeError('x.toUpperCase is not a function')
  )
  const materialized = of(notifA, notifB, notifE)
  const upperCase = materialized.pipe(dematerialize())
  upperCase.subscribe(
    x => print('Dematerialize: ', x),
    e => print('Dematerialize: ', e)
  )
}

// Timeout - errors if Observable does not emit a value in given time span.
// https://rxjs-dev.firebaseapp.com/api/operators/timeout

const Timeout = () => {
  const seconds = interval(100).pipe(take(3))

  seconds
    .pipe(timeout(200)) // Let's use bigger timespan to be safe,
    // since `interval` might fire a bit later then scheduled.
    .subscribe(
      value => console.log(value), // Will emit numbers just as regular `interval` would.
      err => console.log(err) // Will never be called.
    )

  seconds.pipe(timeout(90)).subscribe(
    value => print('Timeout: ', value), // Will never be called.
    err => print('Timeout: ', err) // Will emit error before even first value is emitted,
    // since it did not arrive within 900ms period.
  )
}

// Using - creates an Observable that uses a resource which will
// be disposed at the same time as the Observable.
// https://rxjs-dev.firebaseapp.com/api/operators/using

const Using = () => {
  const seconds = interval(100).pipe(take(3))

  seconds
    .pipe(timeout(200)) // Let's use bigger timespan to be safe,
    // since `interval` might fire a bit later then scheduled.
    .subscribe(
      value => console.log(value), // Will emit numbers just as regular `interval` would.
      err => console.log(err) // Will never be called.
    )

  seconds.pipe(timeout(90)).subscribe(
    value => print('Timeout: ', value), // Will never be called.
    err => print('Timeout: ', err) // Will emit error before even first value is emitted,
    // since it did not arrive within 900ms period.
  )
}

Delay()
Materialize()
Dematerialize()
Timeout()
Using()
